package com.tenniscourts;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import com.tenniscourts.schedules.ScheduleDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDateTime;
import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
public class TennisCourtControllerTest {

  @Mock
  private TennisCourtService service;
  @InjectMocks
  private TennisCourtController controller;


  @Before
  public void setUp() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
  }

  @Test
  public void addTennisCourtTest() {

    TennisCourtDTO actual = TennisCourtDTO.builder()
        .name("Cristi")
        .tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
            .startDateTime(LocalDateTime.now())
            .tennisCourtId(1L)
            .build()))
        .build();

    when(service.addTennisCourt(actual)).thenReturn(actual);
    ResponseEntity<String> expected = controller.addTennisCourt(actual);
    assertEquals(expected.getStatusCodeValue(), 201);
  }

  @Test
  public void findTennisCourtById() {

    TennisCourtDTO actual = TennisCourtDTO.builder()
        .name("Cristi")
        .tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
            .startDateTime(LocalDateTime.now())
            .tennisCourtId(1L)
            .build()))
        .build();

    when(service.findTennisCourtById(anyLong())).thenReturn(actual);
    ResponseEntity<TennisCourtDTO> expected = controller.findTennisCourtById(anyLong());
    assertEquals(expected.getBody().getName(), actual.getName());

  }

  @Test
  public void findTennisCourtWithSchedulesById() {

    TennisCourtDTO actual = TennisCourtDTO.builder()
        .name("Cristi")
        .tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
            .startDateTime(LocalDateTime.now())
            .tennisCourtId(1L)
            .build()))
        .build();

    when(service.findTennisCourtWithSchedulesById(anyLong())).thenReturn(actual);
    ResponseEntity<TennisCourtDTO> expected = controller.findTennisCourtWithSchedulesById(anyLong());
    assertEquals(expected.getBody().getTennisCourtSchedules().get(0).getTennisCourtId(),
        actual.getTennisCourtSchedules().get(0).getTennisCourtId());

  }
}
