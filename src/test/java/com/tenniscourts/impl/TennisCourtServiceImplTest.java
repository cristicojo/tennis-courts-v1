package com.tenniscourts.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.tenniscourts.TennisCourt;
import com.tenniscourts.TennisCourtDTO;
import com.tenniscourts.TennisCourtMapper;
import com.tenniscourts.TennisCourtRepository;
import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.schedules.Schedule;
import com.tenniscourts.schedules.ScheduleDTO;
import com.tenniscourts.schedules.ScheduleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class TennisCourtServiceImplTest {

	@Mock
	private TennisCourtRepository tennisCourtRepository;

	@Mock
	private ScheduleService scheduleService;

	@Mock
	private TennisCourtMapper tennisCourtMapper;


	@InjectMocks
	private TennisCourtServiceImpl tennisCourtService;



	@Test
	public void addTennisCourtTest(){

		TennisCourt tennisCourt = TennisCourt.builder()
				.name("Cristi")
				.scheduleList(Collections.singletonList(Schedule.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder()
				.name("Cristi")
				.tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		when(tennisCourtMapper.map(tennisCourtDTO)).thenReturn(tennisCourt);
		when(tennisCourtRepository.saveAndFlush(tennisCourt)).thenReturn(tennisCourt);
		when(tennisCourtMapper.map(tennisCourt)).thenReturn(tennisCourtDTO);

		assertThat(tennisCourtService.addTennisCourt(tennisCourtDTO)).isSameAs(tennisCourtDTO);
		verify(tennisCourtRepository, times(1)).saveAndFlush(tennisCourt);
	}

	@Test
	public void findTennisCourtByIdTest(){

		TennisCourt tennisCourt = TennisCourt.builder()
				.name("Cristi")
				.scheduleList(Collections.singletonList(Schedule.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder()
				.name("Cristi")
				.tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		when(tennisCourtRepository.findById(anyLong())).thenReturn(Optional.of(tennisCourt));
		when(tennisCourtMapper.map(tennisCourt)).thenReturn(tennisCourtDTO);

		assertEquals(tennisCourtService.findTennisCourtById(anyLong()), tennisCourtDTO);
		verify(tennisCourtMapper,times(1)).map(tennisCourt);
	}

	@Test(expected = EntityNotFoundException.class)
	public void findTennisCourtByIdNegativeTest(){

		when(tennisCourtRepository.findById(anyLong())).thenReturn(Optional.empty());
		tennisCourtService.findTennisCourtById(anyLong());
		verify(tennisCourtRepository,times(1)).findById(anyLong());
	}

	@Test
	public void findTennisCourtWithSchedulesByIdTest() {

		TennisCourt tennisCourt = TennisCourt.builder()
				.name("Cristi")
				.scheduleList(Collections.singletonList(Schedule.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder()
				.name("Cristi")
				.tennisCourtSchedules(Collections.singletonList(ScheduleDTO.builder()
						.startDateTime(LocalDateTime.now())
						.tennisCourtId(1L)
						.build())).build();

		when(tennisCourtRepository.findById(anyLong())).thenReturn(Optional.of(tennisCourt));
		when(tennisCourtMapper.map(tennisCourt)).thenReturn(tennisCourtDTO);
		when(scheduleService.findSchedulesByTennisCourtId(anyLong())).thenReturn(tennisCourtDTO.getTennisCourtSchedules());

		assertEquals(tennisCourtService.findTennisCourtWithSchedulesById(anyLong()),tennisCourtDTO);
		verify(tennisCourtMapper,times(1)).map(tennisCourt);

	}
}
