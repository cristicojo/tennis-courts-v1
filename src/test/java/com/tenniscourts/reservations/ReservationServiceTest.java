package com.tenniscourts.reservations;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.schedules.Schedule;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = ReservationService.class)
public class ReservationServiceTest {

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private ReservationMapper reservationMapper;


    @InjectMocks
    private ReservationService reservationService;

    @Test
    public void getRefundValueFullRefund() {
        Schedule schedule = new Schedule();

        LocalDateTime startDateTime = LocalDateTime.now().plusDays(2);

        schedule.setStartDateTime(startDateTime);

        Assert.assertEquals(reservationService.getRefundValue(Reservation.builder().schedule(schedule).value(new BigDecimal(10L)).build()), new BigDecimal(10));
    }

    @Test
    public void getRefundValueZero() {
        Schedule schedule = new Schedule();

        LocalDateTime startDateTime = LocalDateTime.now();

        schedule.setStartDateTime(startDateTime);

        assertEquals(reservationService.getRefundValue(Reservation.builder().schedule(schedule).value(new BigDecimal(0L)).build()), new BigDecimal(0));
    }

    @Test
    public void findReservationTest(){

        Reservation reservation = Reservation.builder().guestId(1L).scheduleId(1L).refundValue(new BigDecimal(1)).build();
        ReservationDTO reservationDTO = ReservationDTO.builder().guestId(1L).scheduledId(1L).refundValue(new BigDecimal(1L)).build();

        when(reservationRepository.findById(anyLong())).thenReturn(Optional.of(reservation));
        when(reservationMapper.map(reservation)).thenReturn(reservationDTO);

        assertEquals(reservationService.findReservation(anyLong()), reservationDTO);
        verify(reservationMapper,times(1)).map(reservation);
    }

    @Test(expected = EntityNotFoundException.class)
    public void findReservationNegativeTest(){

        when(reservationRepository.findById(anyLong())).thenReturn(Optional.empty());
        reservationService.findReservation(anyLong());
        verify(reservationRepository,times(1)).findById(anyLong());
    }
}