package com.tenniscourts;

public interface TennisCourtService {

  TennisCourtDTO addTennisCourt(TennisCourtDTO tennisCourt);

  TennisCourtDTO findTennisCourtById(Long id);

  TennisCourtDTO findTennisCourtWithSchedulesById(Long tennisCourtId);

}
