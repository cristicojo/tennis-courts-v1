package com.tenniscourts.impl;

import com.tenniscourts.TennisCourtDTO;
import com.tenniscourts.TennisCourtMapper;
import com.tenniscourts.TennisCourtRepository;
import com.tenniscourts.TennisCourtService;
import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.schedules.ScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
@AllArgsConstructor
public class TennisCourtServiceImpl implements TennisCourtService {

  private final TennisCourtRepository tennisCourtRepository;

  private final ScheduleService scheduleService;

  private final TennisCourtMapper tennisCourtMapper;


  @Override
  public TennisCourtDTO addTennisCourt(TennisCourtDTO tennisCourt) {
    return tennisCourtMapper.map(
        tennisCourtRepository.saveAndFlush(tennisCourtMapper.map(tennisCourt)));
  }

  @Override
  public TennisCourtDTO findTennisCourtById(Long id) {
    return tennisCourtRepository.findById(id).map(tennisCourtMapper::map).orElseThrow(() ->
        new EntityNotFoundException("Tennis Court not found."));

  }

  @Override
  public TennisCourtDTO findTennisCourtWithSchedulesById(Long tennisCourtId) {
    TennisCourtDTO tennisCourtDTO = findTennisCourtById(tennisCourtId);
    tennisCourtDTO.setTennisCourtSchedules(
        scheduleService.findSchedulesByTennisCourtId(tennisCourtId));
    return tennisCourtDTO;
  }



}
